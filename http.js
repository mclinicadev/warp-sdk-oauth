// References
var Promise = require('promise');
var rp = require('request-promise');
var _ = require('underscore');
var WarpError = require('./error');
var Storage = require('./storage');

module.exports = {
    extend: function() {
        // Class constructor
        var Http = {
            _baseURL: 'api/1/',
            _apiKey: null,
            _timeout: 10,
            _storage: null,
            _formatQueryString: function(args) {
                var parsed = [];
                for(var item in args)
                {
                    var value = args[item];

                    if(typeof value === 'undefined' || typeof value === 'null')
                        continue;
                    else if(typeof value === 'object')
                        if(typeof value.map !== 'function' && Object.keys(value).length == 0)
                            continue;
                        else
                            value = JSON.stringify(value);

                    parsed.push(encodeURIComponent(item) + '=' + encodeURIComponent(value));
                }
                return parsed.join('&');
            },
            _urlEncodeJsObject: function(obj) {
              function encodeKeyVal(key, val) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(val) + '&';
              }

              var encodedString = '';

              for(var prop in obj) {
                if(obj.hasOwnProperty(prop)) {
                  if(Array.isArray(obj[prop])) {
                    obj[prop].forEach(function(val) {
                      encodedString += encodeKeyVal(prop, val);
                    });
                  } else {
                    encodedString += encodeKeyVal(prop, obj[prop]);
                  }
                }
              }

              return encodedString;
            },
            _refreshOauthToken: function(opts) {
                opts = opts || {};
                var uri = this._baseURL.replace(/\/$/, "") + '/oauth/token';

                var form = {
                    grant_type: 'refresh_token',
                    client_id: opts.client_id || 'default',
                    refresh_token: this.getRefreshToken(),
                }

                return rp({
                    method: 'POST',
                    uri: uri,
                    form: form,
                    json: true,
                })
            },
            _request: function(method, url, args, isRaw, isUrlFormEncoded) {
                var _self = this;
                // Make sure configurations are made
                if(!method || !url)
                    throw new WarpError(WarpError.Code.MissingConfiguration, 'Missing url and/or HTTP method');

                // Prepare url and method
                url = this._baseURL.replace(/\/$/, "") + '/' + url;
                method = method.toUpperCase();


                var headers = {
                    'X-Warp-API-Key': this._apiKey,
                    'X-Warp-Session-Token': this.getSessionToken(),
                    'Authorization': 'Bearer ' + this.getAccessToken(),
                }

                var options = null;
                if(isUrlFormEncoded) {
                    options = {
                        method: 'POST',
                        uri: url,
                        resolveWithFullResponse: true,
                        headers: headers,
                        form: args,
                        json: true
                    };
                } else if(isRaw) {
                    options = {
                        method: 'POST',
                        formData: args,
                        resolveWithFullResponse: true,
                        headers: headers,
                        json: true
                    };
                } else if(method === 'GET'){
                    options = {
                        method: method,
                        uri: url + '?' + this._formatQueryString(args),
                        resolveWithFullResponse: true,
                        headers: headers,
                        json: true
                    };
                } else {
                    options = {
                        method: method,
                        uri: url,
                        resolveWithFullResponse: true,
                        headers: headers,
                        body: _.includes(['POST', 'PUT'], method) ? args : null,
                        json: true
                    };
                }

                console.log(method, url, args, isRaw, isUrlFormEncoded);

                return new Promise(function (resolve, reject) {
                    return rp(options).then(function(response) {
                        var body = response.body;
                        return resolve(body.result || body)
                    }).catch(function(error) {
                        if(error.statusCode === 401) {
                            return _self._refreshOauthToken().then(function(refreshTokenResult) {
                                var response = refreshTokenResult;
                                _self.setAccessToken(response.accessToken);
                                _self.setRefreshToken(response.refreshToken);

                                options.headers.Authorization = 'Bearer ' + response.accessToken;
                                return rp(options).then(function(response2){
                                    var body = response2.body;
                                    return resolve(body.result || body)
                                }).catch(function(error) {
                                    return reject(error.error)
                                })
                            })
                        }
                        return reject(error.error);
                    })
                });

                // Return request
                /*return new Promise(function (resolve, reject) {
                    var promise = this;
                    // Instantiate the XMLHttpRequest
                    var client = new XMLHttpRequest();
                    var params = '';
                    var handled = false;
                    var timedOut = false;

                    // Add timeout checker
                    var timeoutChecker = setTimeout(function() {
                        // Return with a network timeout error
                        reject(new WarpError(800, 'Network timeout'));

                        // Change timed out status
                        timedOut = true;
                    }, this._timeout * 1000); // timeout (in seconds)

                    // Set onload method
                    client.onreadystatechange = function() {
                        // Check readyState
                        if (client.readyState !== 4 || handled || timedOut) return;
                        handled = true;

                        // Stop the timeout
                        clearTimeout(timeoutChecker);
                        if(this.status === 401 && this.responseText.includes('Invalid token: access token has expired')) {
                            return _self._refreshOauthToken().then((refreshTokenResult) => {
                                debugger;
                                try {
                                    var response = JSON.parse(refreshTokenResult);
                                    _self.setAccessToken(response.accessToken);
                                    _self.setRefreshToken(response.refreshToken);
                                    client.send(params);
                                } catch(err) {
                                    error = new WarpError(700, 'Network not found');
                                    return reject(error);
                                }
                            })
                        }

                        if(this.status >= 200 && this.status < 300) {
                            var response = JSON.parse(this.responseText).result || JSON.parse(this.responseText);
                            return resolve(response);
                        } else
                        {
                            try
                            {
                                var response = JSON.parse(this.responseText);
                                var error = new WarpError(response.status, response.message);
                                return reject(error);
                            }
                            catch(err)
                            {
                                var error = new WarpError(700, 'Network not found');
                                return reject(error);
                            }
                        }
                    };

                    // Check method
                    if(method === 'GET')
                    {
                        var parsed = [];
                        for(var item in args)
                        {
                            var value = args[item];

                            if(typeof value === 'undefined' || typeof value === 'null')
                                continue;
                            else if(typeof value === 'object')
                                if(typeof value.map !== 'function' && Object.keys(value).length == 0)
                                    continue;
                                else
                                    value = JSON.stringify(value);

                            parsed.push(encodeURIComponent(item) + '=' + encodeURIComponent(value));
                        }
                        url += '?' + parsed.join('&');
                    }

                    // Prepare client
                    client.open(method, url, true);
                    client.setRequestHeader('X-Warp-API-Key', this._apiKey);
                    client.setRequestHeader('X-Warp-Session-Token', this.getSessionToken());
                    client.setRequestHeader('Authorization', 'Bearer ' + this.getAccessToken());

                    if(!isRaw)
                    {
                        if(method === 'POST' || method === 'PUT')
                        {
                            if(!isUrlFormEncoded) {
                                client.setRequestHeader('Content-Type', 'application/json');
                                params = args? JSON.stringify(args) : null;
                            } else {
                                client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                                params = this._urlEncodeJsObject(args);
                            }
                        }
                        else
                            client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    }
                    else
                    {
                        params = args;
                    }

                    // Send the request
                    client.send(params);
                }.bind(this));*/
            },
            initialize: function(config) {
                if(!config.apiKey) throw new WarpError(WarpError.Code.MissingConfiguration, 'API Key must be set');
                this._apiKey = config.apiKey;
                this._baseURL = config.baseURL || this._baseURL;
                this._timeout = config.timeout || this._timeout;
                this._storage = Storage.extend({
                    baseURL: this._baseURL
                });
            },
            setSessionToken: function(sessionToken) {
                this._storage.setItem('x-warp-session-token', sessionToken);
            },
            unsetSessionToken: function() {
                this._storage.removeItem('x-warp-session-token');
            },
            getSessionToken: function() {
                return this._storage.getItem('x-warp-session-token');
            },
            setAccessToken: function(accessToken) {
                this._storage.setItem('oauth-access-token', accessToken);
            },
            unsetAccessToken: function() {
                this._storage.removeItem('oauth-access-token');
            },
            getAccessToken: function() {
                return this._storage.getItem('oauth-access-token');
            },
            setRefreshToken: function(refreshToken) {
                this._storage.setItem('oauth-refresh-token', refreshToken);
            },
            unsetRefreshToken: function() {
                this._storage.removeItem('oauth-refresh-token');
            },
            getRefreshToken: function() {
                return this._storage.getItem('oauth-refresh-token');
            },
            find: function(endpoint, args) {
                return this._request('GET', endpoint, args);
            },
            first: function(endpoint, id) {
                return this._request('GET', endpoint + '/' + id);
            },
            create: function(endpoint, args) {
                return this._request('POST', endpoint, args);
            },
            update: function(endpoint, id, args) {
                return this._request('PUT', endpoint + '/' + id, args);
            },
            destroy: function(endpoint, id) {
                return this._request('DELETE', endpoint + '/' + id);
            },
            run: function(endpoint, args) {
                return this._request('POST', endpoint, args);
            },
            upload: function(args) {
                return this._request('POST', 'files', args, true);
            },
            logIn: function(args) {
                return this._request('POST', 'login', args);
            },
            oAuthLogin: function(args) {
                return this._request('POST', 'oauth/token', args, false, true);
            },
            logOut: function() {
                return this._request('GET', 'logout');
            },
            current: function() {
                return this._request('GET', 'users/me');
            }
        };

        return Http;
    }
};